#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libcollection/types.h>
#include <libjavascript/all.h>
#include <libtest/libtest.h>
#include "libjson.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
void test_null();
void test_string();
void test_number();
void test_boolean();
void test_object();
void test_array();

/* Global Variables ***********************************************************/

int main(int argc, char *argv[]) {
  test_init("Parsers", argc, argv);

  test("Null", test_null);
  test("String", test_string);
  test("Number", test_number);
  test("Boolean", test_boolean);
  test("Object", test_object);
  test("Array", test_array);

  return test_complete();
}

void test_null() {
  JSValue *value = json_parse("null");

  test_assert(value->type == JSNull, "Should return null type");
  test_assert(value->value == NULL, "Should return null value");
}

void test_string() {
  JSValue *value = json_parse("\"value\"");

  test_assert(value->type == JSString, "Should return string type");
  test_assert(!strcmp(value->value, "value"), "Should return string value");
}

void test_number() {
  JSValue *value = json_parse("123");

  test_assert(value->type == JSNumber, "Should return number type");
  test_assert(value->value == 123, "Should return number value");
}

void test_boolean() {
  JSValue *value = json_parse("true");

  test_assert(value->type == JSBoolean, "Should return boolean type");
  test_assert(value->value == TRUE, "Should return boolean value");
}

void test_object() {
  JSValue *value = json_parse("{ \"property\": \"value\" }");

  test_assert(value->type == JSObject, "Should return Object type");
  test_assert_string(object_get(value, "property")->value, "value", "Should return Object value");
}

void test_array() {
  JSValue *value = json_parse("[ \"hello\" ]");

  test_assert(value->type == JSArray, "Should return Array type");
  test_assert_int(array_length(value), 1, "Should return an array with the same number of items");
  test_assert_string(array_get(value, 0)->value, "hello", "Should return Array value");
}
