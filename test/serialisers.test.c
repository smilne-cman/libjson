#include <stdio.h>
#include <stdlib.h>
#include <libtest/libtest.h>
#include <libjavascript/all.h>
#include "libjson.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
void test_c_null();
void test_undefined();
void test_null();
void test_string();
void test_number();
void test_boolean();
void test_object();
void test_array();

/* Global Variables ***********************************************************/

int main(int argc, char *argv[]) {
  test_init("Serialisers", argc, argv);

  test("C NULL",  test_c_null);
  test("Undefined", test_undefined);
  test("Null", test_null);
  test("String", test_string);
  test("Number", test_number);
  test("Boolean", test_boolean);
  test("Object", test_object);
  test("Array", test_array);

  return test_complete();
}

void test_c_null() {
  test_assert_string(json_stringify(NULL), "", "Should return empty string if given NULL");
}

void test_undefined() {
  JSValue *value = js_undefined();

  test_assert_string(
    json_stringify(value),
    "undefined",
    "Should stringify undefined"
  );
}

void test_null() {
  JSValue *value = js_null();

  test_assert_string(
    json_stringify(value),
    "null",
    "Should stringify null"
  );
}

void test_string() {
  JSValue *value = js_string("Hello world!");

  test_assert_string(
    json_stringify(value),
    "\"Hello world!\"",
    "Should stringify strings"
  );
}

void test_number() {
  JSValue *value = js_number(123);

  test_assert_string(
    json_stringify(value),
    "123",
    "Should stringify numbers"
  );
}

void test_boolean() {
  JSValue *value = js_boolean(TRUE);

  test_assert_string(
    json_stringify(value),
    "true",
    "Should stringify booleans"
  );
}

void test_object() {
  JSValue *value = js_object();
  object_put(value, "property", js_string("value"));

  test_assert_string(
    json_stringify(value),
    "{ \"property\": \"value\" }",
    "Should stringify objects"
  );
}

void test_array() {
  JSValue *value = js_array();
  array_push(value, js_string("value"));

  test_assert_string(
    json_stringify(value),
    "[ \"value\" ]",
    "Should stringify arrays"
  );
}
