#include <stdio.h>
#include <stdlib.h>
#include <libtest/libtest.h>
#include <libjavascript/all.h>
#include "libjson.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
void test_token();

/* Global Variables ***********************************************************/

int main(int argc, char *argv[]) {
  test_init("Utilities", argc, argv);

  test("Test Token", test_token);

  return test_complete();
}

void test_token() {
  JSValue *object = js_object();
  char *json = "{ \"message\": \"Hello world!\" }";
  JSONToken *token = json_token(json, 0, 32, object);

  test_assert(!strcmp(token->json, json), "Should set a reference to the source JSON");
  test_assert_int(token->start, 0, "Should set the start offset");
  test_assert_int(token->end, 32, "Should set the end offset");
  test_assert(token->value == object, "Should set the JS value");
}
