#include <stdio.h>
#include <stdlib.h>
#include <libcollection/iterator.h>
#include <libcollection/list.h>
#include <libcollection/map.h>
#include "libjson.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
char *stringify_undefined(JSValue *value);
char *stringify_null(JSValue *value);
char *stringify_string(JSValue *object);
char *stringify_number(JSValue *object);
char *stringify_boolean(JSValue *object);
char *stringify_object(JSValue *object);
char *stringify_array(JSValue *object);

/* Global Variables ***********************************************************/
static const int MAX_NUMBER_LENGTH = 18;
static const int MAX_OBJECT_LENGTH = 2048;
static const int MAX_ARRAY_LENGTH = 2048;

/* Functions ******************************************************************/
char *json_stringify(JSValue *value) {
  if (value == NULL) return "";

  switch (value->type) {
    case JSUndefined:
      return stringify_undefined(value);
    case JSNull:
      return stringify_null(value);
    case JSString:
      return stringify_string(value);
    case JSNumber:
      return stringify_number(value);
    case JSBoolean:
      return stringify_boolean(value);
    case JSObject:
      return stringify_object(value);
    case JSArray:
      return stringify_array(value);
    default:
      return "Javascript error: Not a valid Javascript type.";
  }
}

char *stringify_undefined(JSValue *value) {
  char *string = (char *)malloc(sizeof(char) * 25);

  sprintf(string, "%s", JS_UNDEFINED);

  return string;
}

char *stringify_null(JSValue *value) {
  char *string = (char *)malloc(sizeof(char) * 5);

  sprintf(string, "%s", JS_NULL);

  return string;
}

char *stringify_string(JSValue *object) {
  char *string = (char *)malloc(sizeof(char) * strlen(object->value));

  sprintf(string, "\"%s\"", object->value);

  return string;
}

char *stringify_number(JSValue *object) {
  char *string = (char *)malloc(sizeof(char) * MAX_NUMBER_LENGTH);

  sprintf(string, "%lu", object->value);

  return string;
}

char *stringify_boolean(JSValue *object) {
  char *string = (char *)malloc(sizeof(char) * 6);
  int *value = object->value;

  sprintf(string, value ? "true" : "false");

  return string;
}

char *stringify_object(JSValue *object) {
  char *string = (char *)malloc(sizeof(char) * MAX_OBJECT_LENGTH);
  sprintf(string, "{");

  int first = TRUE;
  Map *properties = (Map *)object->value;
  Iterator *iterator = map_iterator(properties);
  while(has_next(iterator)) {
    MapEntry *entry = next_map(iterator);

    JSValue *key = js_string(entry->key);
    JSValue *value = (JSValue *)entry->value;

    if (value->type == JSUndefined) {
      continue;
    }

    if (first) {
      first = FALSE;
      sprintf(string, "%s %s: %s", string, json_stringify(key), json_stringify(value));
    } else {
      sprintf(string, "%s, %s: %s", string, json_stringify(key), json_stringify(value));
    }
  }

  sprintf(string, "%s }", string);
  return string;
}

char *stringify_array(JSValue *object) {
  char *string = (char *)malloc(sizeof(char) * MAX_ARRAY_LENGTH);
  sprintf(string, "[");

  int first = TRUE;
  List *items = (List *)object->value;
  Iterator *iterator = list_iterator(items);
  while(has_next(iterator)) {
    JSValue *item = (JSValue *)next(iterator);

    if (first) {
      first = FALSE;
      sprintf(string, "%s %s", string, json_stringify(item));
    } else {
      sprintf(string, "%s, %s", string, json_stringify(item));
    }
  }

  sprintf(string, "%s ]", string);
  return string;
}
