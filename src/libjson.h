#ifndef __libjson__
#define __libjson__

/* Includes *******************************************************************/
#include <libjavascript/types.h>

/* Types **********************************************************************/
typedef struct JSONToken {
  const char *json;
  JSValue *value;
  int start;
  int end;
} JSONToken;

typedef JSONToken *(*Parser)(char *json, int offset);

/* Macros *********************************************************************/

/* Global Functions ***********************************************************/
char *json_stringify(JSValue *value);
JSValue *json_parse(const char *json);

JSONToken *json_token(char *json, int start, int end, JSValue *value);

#endif
