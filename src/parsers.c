#include <stdio.h>
#include <stdlib.h>
#include <libstring/libstring.h>
#include "libjson.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
static Parser getParser(char firstCharacter);
static JSONToken *parse_null(char *json, int offset);
static JSONToken *parse_object(char *json, int offset);
static JSONToken *parse_array(char *json, int offset);
static JSONToken *parse_string(char *json, int offset);
static JSONToken *parse_boolean(char *json, int offset);
static JSONToken *parse_number(char *json, int offset);

static int parse_property(char *json, int offset, JSValue *object);
static int is_number(char character);

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
JSValue *json_parse(const char *json) {
  char *trimmed_json = json[0] == ' ' ? trim(json) : json;
  Parser parser = getParser(trimmed_json[0]);

  return parser == NULL ? js_null() : parser(trimmed_json, 0)->value;
}

static Parser getParser(char firstCharacter) {
  switch (firstCharacter) {
    case '{':
      return parse_object;
    case '[':
      return parse_array;
    case '"':
      return parse_string;
    case 't':
    case 'f':
      return parse_boolean;
    case 'n':
      return parse_null;
    case '0':
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
      return parse_number;
    default:
      return NULL;
  }
}

static JSONToken *parse_null(char *json, int offset) {
  // FIXME -- This will incorrectly identify "nul", "nu" and "n" and will mess up the offset
  return json_token(json, offset, offset + 3, js_null());
}

static JSONToken *parse_object(char *json, int offset) {
  JSValue *object = js_object();
  int length = strlen(json);
  int pointer = offset;

  while(json[pointer] != '}' && pointer <= length) {
    char current = json[pointer];

    if (current != ' ' && current != '\n' &&  current != ',' && current != ':' && current != '{') {
      pointer = parse_property(json, pointer, object);
    }

    pointer++;
  }

  return json_token(json, offset, pointer + 1, object);
}

static int parse_property(char *json, int offset, JSValue *object) {
  JSONToken *key_token = parse_string(json, offset);
  if (key_token == NULL) {
    return key_token->end + 1;
  }

  int length = strlen(json);
  int pointer = key_token->end + 1;

  while(json[pointer] != ',' && pointer <= length) {
    char current = json[pointer];

    if (current != ' ' && current != '\n') {
      Parser parser = getParser(current);

      if (parser != NULL) {
        JSONToken *token = parser(json, pointer);
        JSValue *value = token->value;

        if (value != NULL && value->type != JSUndefined) {
          object_put(object, key_token->value->value, value);
        }

        pointer = token->end;
        break;
      }
    }

    pointer++;
  }

  return pointer + 1;
}

static JSONToken *parse_array(char *json, int offset) {
  JSValue *array = js_array();
  int length = strlen(json);
  int pointer = offset + 1;

  while(json[pointer] != ']' && pointer <= length) {
    char current = json[pointer];

    if (current != ' ' && current != '\n' &&  current != ',') {
      Parser parser = getParser(current);

      if (parser != NULL) {
        JSONToken *token = parser(json, pointer);
        JSValue *value = token->value;

        if (value != NULL && value->type != JSUndefined) {
          array_push(array, value);
        }

        pointer = token->end;
      }
    }

    pointer++;
  }

  return json_token(json, offset, offset, array);
}

static JSONToken *parse_string(char *json, int offset) {
  int length = strlen(json);
  int pointer = offset + 1;

  while(json[pointer] != '"' && pointer <= length) {
    pointer++;
  }

  // FIXME -- Won't handle backslash quotes in the string!
  JSValue *value = js_string(substring(json, offset + 1, pointer -  1));

  return json_token(json, offset, pointer, value);
}

static JSONToken *parse_boolean(char *json, int offset) {
  int isTrue = starts_with("true", json + offset);
  int isFalse = starts_with("false", json + offset);

  int length = 0;
  if (isTrue) {
    length = 3;
  } else if (isFalse) {
    length = 4;
  }

  return json_token(json, offset, offset + length, length > 0 ? js_boolean(isTrue) : js_undefined());
}

static JSONToken *parse_number(char *json, int offset) {
  int length = strlen(json);
  int pointer = offset;

  // FIXME -- Won't handle floats
  while(is_number(json[pointer]) && pointer <= length) {
    pointer++;
  }

  long value = strtol(json + offset, NULL, 10);
  return json_token(json, offset, pointer - 1, js_number(value));
}

static int is_number(char character) {
  return character == '0'
    | character == '1'
    | character == '2'
    | character == '3'
    | character == '4'
    | character == '5'
    | character == '6'
    | character == '7'
    | character == '8'
    | character == '9';
}
