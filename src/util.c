#include <stdio.h>
#include <stdlib.h>
#include "libjson.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
JSONToken *json_token(char *json, int start, int end, JSValue *value) {
  JSONToken *token = (JSONToken *)malloc(sizeof(JSONToken));

  token->json = json;
  token->value = value;
  token->start = start;
  token->end = end;

  return token;
}
